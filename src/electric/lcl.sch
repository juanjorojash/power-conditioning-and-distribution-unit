EESchema Schematic File Version 4
LIBS:pcdu-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 16 35
Title ""
Date ""
Rev ""
Comp "LibreCube"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3050 3200 0    60   Input ~ 0
IN
Text HLabel 5600 3250 2    60   Input ~ 0
OUT
Text HLabel 3050 3950 0    60   Input ~ 0
ON
$Sheet
S 4100 3100 900  400 
U 59842ECB
F0 "lcl_switch_1" 60
F1 "switch.sch" 60
F2 "OUT" O R 5000 3250 60 
F3 "IN" I L 4100 3200 60 
F4 "EN" I L 4100 3300 60 
F5 "/OC" O R 5000 3400 60 
$EndSheet
$Sheet
S 4100 3750 900  400 
U 59842FE8
F0 "lcl_switch_2" 60
F1 "switch.sch" 60
F2 "OUT" O R 5000 3900 60 
F3 "IN" I L 4100 3850 60 
F4 "EN" I L 4100 3950 60 
F5 "/OC" O R 5000 4050 60 
$EndSheet
Wire Wire Line
	3050 3200 4100 3200
Wire Wire Line
	4100 3850 3900 3850
Wire Wire Line
	3900 3850 3900 3200
Connection ~ 3900 3200
Wire Wire Line
	5000 3900 5300 3900
Wire Wire Line
	5300 3900 5300 3250
Wire Wire Line
	5000 3250 5600 3250
Connection ~ 5300 3250
Wire Wire Line
	3050 3950 4100 3950
Wire Wire Line
	3800 3950 3800 3300
Wire Wire Line
	3800 3300 4100 3300
Connection ~ 3800 3950
$EndSCHEMATC
