EESchema Schematic File Version 4
LIBS:pcdu-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 13 35
Title ""
Date ""
Rev ""
Comp "LibreCube"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3400 4000 0    60   Input ~ 0
IN
Text HLabel 5650 4050 2    60   Output ~ 0
OUT
$Sheet
S 4100 3900 1000 400 
U 5983F4B3
F0 "fcl_switch_1" 60
F1 "switch.sch" 60
F2 "OUT" O R 5100 4050 60 
F3 "IN" I L 4100 4000 60 
F4 "EN" I L 4100 4150 60 
F5 "/OC" O R 5100 4200 60 
$EndSheet
Wire Wire Line
	3400 4000 4100 4000
Wire Wire Line
	5100 4050 5650 4050
Wire Wire Line
	4100 4150 3850 4150
Wire Wire Line
	3850 4150 3850 4000
Connection ~ 3850 4000
$Sheet
S 4100 4550 1000 400 
U 5983F7D4
F0 "fcl_switch_2" 60
F1 "switch.sch" 60
F2 "OUT" O R 5100 4700 60 
F3 "IN" I L 4100 4650 60 
F4 "EN" I L 4100 4800 60 
F5 "/OC" O R 5100 4850 60 
$EndSheet
Wire Wire Line
	4100 4650 3700 4650
Wire Wire Line
	3700 4650 3700 4000
Connection ~ 3700 4000
Wire Wire Line
	4100 4800 3850 4800
Wire Wire Line
	3850 4800 3850 4650
Connection ~ 3850 4650
Wire Wire Line
	5100 4700 5350 4700
Wire Wire Line
	5350 4700 5350 4050
Connection ~ 5350 4050
$EndSCHEMATC
