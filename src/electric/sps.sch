EESchema Schematic File Version 4
LIBS:pcdu-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 35
Title ""
Date ""
Rev ""
Comp "LibreCube"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4600 2800 0    60   Input ~ 0
IN
Text HLabel 7950 2900 2    60   Input ~ 0
OUT
Text HLabel 2950 1550 0    60   Input ~ 0
S0
Text HLabel 2950 1700 0    60   Input ~ 0
S2
Text HLabel 2950 1850 0    60   Input ~ 0
S4
Text HLabel 2950 2150 0    60   Input ~ 0
S1
Text HLabel 2950 2300 0    60   Input ~ 0
S3
Text HLabel 2950 2450 0    60   Input ~ 0
S5
Text Notes 1750 1600 0    60   ~ 0
RBF normal closed
Text Notes 1750 1750 0    60   ~ 0
RBF normal open
Text Notes 1750 1900 0    60   ~ 0
RBF common
Text Notes 1550 2200 0    60   ~ 0
KillSwitch normal closed
Text Notes 1550 2350 0    60   ~ 0
KillSwitch normal open
Text Notes 1550 2500 0    60   ~ 0
KillSwitch common
$Comp
L pcdu-rescue:GND-power #PWR34
U 1 1 598268C3
P 3150 1850
F 0 "#PWR34" H 3150 1600 50  0001 C CNN
F 1 "GND" H 3150 1700 50  0000 C CNN
F 2 "" H 3150 1850 50  0000 C CNN
F 3 "" H 3150 1850 50  0000 C CNN
	1    3150 1850
	1    0    0    -1  
$EndComp
$Comp
L pcdu-rescue:GND-power #PWR35
U 1 1 598268D5
P 3150 2450
F 0 "#PWR35" H 3150 2200 50  0001 C CNN
F 1 "GND" H 3150 2300 50  0000 C CNN
F 2 "" H 3150 2450 50  0000 C CNN
F 3 "" H 3150 2450 50  0000 C CNN
	1    3150 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2450 3150 2450
Wire Wire Line
	2950 1850 3150 1850
NoConn ~ 3150 2150
NoConn ~ 3150 1550
Wire Wire Line
	3150 1550 2950 1550
Wire Wire Line
	3150 2150 2950 2150
$Comp
L pcdu-rescue:Conn_01x02_Male-Connector P9
U 1 1 598268FA
P 1950 3050
F 0 "P9" H 1950 3200 50  0000 C CNN
F 1 "CONN_01X02" V 2050 3050 50  0000 C CNN
F 2 "" H 1950 3050 50  0000 C CNN
F 3 "" H 1950 3050 50  0000 C CNN
	1    1950 3050
	1    0    0    -1  
$EndComp
$Comp
L pcdu-rescue:GND-power #PWR33
U 1 1 5982694A
P 2150 3150
F 0 "#PWR33" H 2150 2900 50  0001 C CNN
F 1 "GND" H 2150 3000 50  0000 C CNN
F 2 "" H 2150 3150 50  0000 C CNN
F 3 "" H 2150 3150 50  0000 C CNN
	1    2150 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 1700 3250 1700
Wire Wire Line
	3250 3050 2150 3050
Wire Wire Line
	2950 2300 3250 2300
Connection ~ 3250 2300
$Sheet
S 6000 2700 1150 500 
U 5983DD90
F0 "switch_1" 60
F1 "switch.sch" 60
F2 "OUT" O R 7150 2900 60 
F3 "IN" I L 6000 2800 60 
F4 "EN" I L 6000 2950 60 
F5 "/OC" O R 7150 3050 60 
$EndSheet
$Sheet
S 6000 3450 1150 500 
U 5983E4FD
F0 "switch_2" 60
F1 "switch.sch" 60
F2 "OUT" O R 7150 3650 60 
F3 "IN" I L 6000 3550 60 
F4 "EN" I L 6000 3700 60 
F5 "/OC" O R 7150 3800 60 
$EndSheet
Wire Wire Line
	7150 3650 7500 3650
Wire Wire Line
	7500 3650 7500 2900
Wire Wire Line
	7150 2900 7500 2900
Connection ~ 7500 2900
Wire Wire Line
	6000 2800 5750 2800
Wire Wire Line
	5750 2800 5750 3550
Wire Wire Line
	5750 3550 6000 3550
Connection ~ 5750 2800
Wire Wire Line
	6000 2950 5600 2950
Connection ~ 3250 2950
Wire Wire Line
	3250 1700 3250 2300
Wire Wire Line
	6000 3700 5600 3700
Wire Wire Line
	5600 3700 5600 2950
Connection ~ 5600 2950
Text Notes 1450 3100 0    60   ~ 0
Secure Lock
Wire Wire Line
	3250 2300 3250 2950
Wire Wire Line
	7500 2900 7950 2900
Wire Wire Line
	5750 2800 4600 2800
Wire Wire Line
	3250 2950 3250 3050
Wire Wire Line
	5600 2950 3250 2950
$EndSCHEMATC
