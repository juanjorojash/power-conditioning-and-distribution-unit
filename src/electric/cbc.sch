EESchema Schematic File Version 4
LIBS:pcdu-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 32 35
Title "CubeSat Board Connector"
Date ""
Rev ""
Comp "LibreCube"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pcdu-rescue:Conn_02x26_Counter_Clockwise-Connector_Generic H1
U 1 1 59848F6F
P 4000 3550
F 0 "H1" H 4000 4900 50  0000 C CNN
F 1 "CONN_02X26" V 4000 3550 50  0000 C CNN
F 2 "" H 4000 2850 50  0000 C CNN
F 3 "" H 4000 2850 50  0000 C CNN
	1    4000 3550
	1    0    0    -1  
$EndComp
$Comp
L pcdu-rescue:Conn_02x26_Counter_Clockwise-Connector_Generic H2
U 1 1 59848FB8
P 5950 3550
F 0 "H2" H 5950 4900 50  0000 C CNN
F 1 "CONN_02X26" V 5950 3550 50  0000 C CNN
F 2 "" H 5950 2850 50  0000 C CNN
F 3 "" H 5950 2850 50  0000 C CNN
	1    5950 3550
	1    0    0    -1  
$EndComp
Text HLabel 3800 2350 0    60   Input ~ 0
CAN_A_L
Text HLabel 4300 2350 2    60   Input ~ 0
CAN_A_H
Text HLabel 5650 2350 0    60   Input ~ 0
CAN_B_L
Text HLabel 6400 2350 2    60   Input ~ 0
CAN_B_H
$Comp
L pcdu-rescue:+5V-power #PWR92
U 1 1 5984AA91
P 5350 3550
F 0 "#PWR92" H 5350 3400 50  0001 C CNN
F 1 "+5V" H 5350 3690 50  0000 C CNN
F 2 "" H 5350 3550 50  0000 C CNN
F 3 "" H 5350 3550 50  0000 C CNN
	1    5350 3550
	1    0    0    -1  
$EndComp
$Comp
L pcdu-rescue:+BATT-power #PWR94
U 1 1 5984AAA9
P 5350 4550
F 0 "#PWR94" H 5350 4400 50  0001 C CNN
F 1 "+BATT" H 5350 4690 50  0000 C CNN
F 2 "" H 5350 4550 50  0000 C CNN
F 3 "" H 5350 4550 50  0000 C CNN
	1    5350 4550
	1    0    0    -1  
$EndComp
$Comp
L pcdu-rescue:GND-power #PWR93
U 1 1 5984AB2F
P 5350 3850
F 0 "#PWR93" H 5350 3600 50  0001 C CNN
F 1 "GND" H 5350 3700 50  0000 C CNN
F 2 "" H 5350 3850 50  0000 C CNN
F 3 "" H 5350 3850 50  0000 C CNN
	1    5350 3850
	1    0    0    -1  
$EndComp
Text HLabel 3800 2450 0    60   Input ~ 0
FCL1
Text HLabel 3800 2550 0    60   Input ~ 0
FCL2
NoConn ~ 3800 3150
NoConn ~ 3800 3250
NoConn ~ 3800 3350
NoConn ~ 3800 3450
NoConn ~ 3800 3550
NoConn ~ 3800 3650
NoConn ~ 3800 3750
NoConn ~ 3800 3850
NoConn ~ 3800 3950
NoConn ~ 3800 4050
NoConn ~ 3800 4150
NoConn ~ 3800 4250
NoConn ~ 3800 4350
NoConn ~ 3800 4450
NoConn ~ 3800 4550
NoConn ~ 3800 4650
NoConn ~ 3800 4750
NoConn ~ 3800 4850
NoConn ~ 4300 3150
NoConn ~ 4300 3250
NoConn ~ 4300 3350
NoConn ~ 4300 3450
NoConn ~ 4300 3550
NoConn ~ 4300 3650
NoConn ~ 4300 3750
NoConn ~ 4300 3950
NoConn ~ 4300 4050
NoConn ~ 4300 4150
NoConn ~ 4300 4250
NoConn ~ 4300 4350
NoConn ~ 4300 4550
NoConn ~ 4300 4650
NoConn ~ 4300 4750
NoConn ~ 4300 4850
Text HLabel 5750 2450 0    60   Input ~ 0
LCL1
Text HLabel 5750 2550 0    60   Input ~ 0
LCL2
Text HLabel 5750 2650 0    60   Input ~ 0
LCL3
Text HLabel 5750 2750 0    60   Input ~ 0
LCL4
Text HLabel 5750 2850 0    60   Input ~ 0
LCL5
NoConn ~ 5750 3450
NoConn ~ 6250 3450
Text HLabel 5750 3950 0    60   Input ~ 0
S0
Text HLabel 5750 4050 0    60   Input ~ 0
S1
Text HLabel 5750 4150 0    60   Input ~ 0
S2
Text HLabel 5750 4250 0    60   Input ~ 0
S3
Text HLabel 5750 4350 0    60   Input ~ 0
S4
Text HLabel 5750 4450 0    60   Input ~ 0
S5
NoConn ~ 5750 3650
NoConn ~ 6250 3650
NoConn ~ 5750 4650
NoConn ~ 5750 4750
NoConn ~ 5750 4850
NoConn ~ 6250 4650
NoConn ~ 6250 4750
NoConn ~ 6250 4850
Text HLabel 4500 4450 2    60   Input ~ 0
CHARGE
Text HLabel 3800 2650 0    60   Input ~ 0
SEL_PCDU_RT_A
Text HLabel 3800 2750 0    60   Input ~ 0
SEL_PCDU_RT_B
NoConn ~ 4300 2850
NoConn ~ 4300 2950
NoConn ~ 4300 3050
NoConn ~ 3800 2850
NoConn ~ 3800 2950
NoConn ~ 3800 3050
NoConn ~ 6250 2950
NoConn ~ 6250 3050
NoConn ~ 6250 3150
NoConn ~ 6250 3250
NoConn ~ 6250 3350
NoConn ~ 5750 2950
NoConn ~ 5750 3050
NoConn ~ 5750 3150
NoConn ~ 5750 3250
NoConn ~ 5750 3350
Text HLabel 6250 3950 2    60   Input ~ 0
S0
Text HLabel 6250 4050 2    60   Input ~ 0
S1
Text HLabel 6250 4150 2    60   Input ~ 0
S2
Text HLabel 6250 4250 2    60   Input ~ 0
S3
Text HLabel 6250 4350 2    60   Input ~ 0
S4
Text HLabel 6250 4450 2    60   Input ~ 0
S5
$Comp
L pcdu-rescue:+BATT-power #PWR97
U 1 1 598567E8
P 6650 4550
F 0 "#PWR97" H 6650 4400 50  0001 C CNN
F 1 "+BATT" H 6650 4690 50  0000 C CNN
F 2 "" H 6650 4550 50  0000 C CNN
F 3 "" H 6650 4550 50  0000 C CNN
	1    6650 4550
	1    0    0    -1  
$EndComp
$Comp
L pcdu-rescue:GND-power #PWR96
U 1 1 59856910
P 6650 3850
F 0 "#PWR96" H 6650 3600 50  0001 C CNN
F 1 "GND" H 6650 3700 50  0000 C CNN
F 2 "" H 6650 3850 50  0000 C CNN
F 3 "" H 6650 3850 50  0000 C CNN
	1    6650 3850
	1    0    0    -1  
$EndComp
$Comp
L pcdu-rescue:+5V-power #PWR95
U 1 1 598569E6
P 6650 3550
F 0 "#PWR95" H 6650 3400 50  0001 C CNN
F 1 "+5V" H 6650 3690 50  0000 C CNN
F 2 "" H 6650 3550 50  0000 C CNN
F 3 "" H 6650 3550 50  0000 C CNN
	1    6650 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3850 5400 3850
Wire Wire Line
	4300 4450 4400 4450
Wire Wire Line
	4400 4450 4400 3850
Wire Wire Line
	4400 3850 4300 3850
Connection ~ 4400 4450
Wire Wire Line
	5350 4550 5750 4550
Wire Wire Line
	6250 4550 6650 4550
Wire Wire Line
	6250 3850 6600 3850
Wire Wire Line
	6250 3750 6600 3750
Wire Wire Line
	6600 3750 6600 3850
Connection ~ 6600 3850
Wire Wire Line
	5750 3750 5400 3750
Wire Wire Line
	5400 3750 5400 3850
Connection ~ 5400 3850
Wire Wire Line
	5350 3550 5750 3550
Wire Wire Line
	6250 3550 6650 3550
Wire Wire Line
	5650 2350 5750 2350
Wire Wire Line
	6250 2350 6400 2350
Wire Wire Line
	4400 4450 4500 4450
Wire Wire Line
	6600 3850 6650 3850
Wire Wire Line
	5400 3850 5750 3850
$EndSCHEMATC
