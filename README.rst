Robust Power Supply System
==========================

This is a prototype of a power conditioning and distribution unit (PCDU) that
implements a full redundancy scheme and provides a CAN bus interface according
to the LibreCube board specification.

Getting Started
---------------

Find in the *src* folder the KiCAD source files of the prototype board.

Testing
-------

In the *tests* folder are evaluation boards for the individual modules of
the prototype for testing of functionality.
